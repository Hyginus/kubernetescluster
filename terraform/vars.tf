variable "region" {
  default     = "us-east-2"
  description = "AWS region"
}

# VCP Varables ################################

variable "vpc_subnet_cidr" {
  default     = "10.0.0.0/16"
  type        = string
  description = "The VPC Subnet CIDR"
}

variable "public_subnet_cidr" {
  default     = ["10.0.1.0/24", "10.0.2.0/24", "10.0.3.0/24"]
  type        = list(string)
  description = "Private Subnet CIDR"
}

variable "private_subnet_cidr" {
  default     = ["10.0.4.0/24", "10.0.5.0/24", "10.0.6.0/24"]
  type        = list(string)
  description = "Public Subnet CIDR"
}

# EKS Variables ###################################

variable "eks_k8s_version" {
  type    = string
  default = "1.20"
}

variable "eks_cluster_name" {
  default = "ydata_cluster"
  type    = string
}

variable "eks_instance_type" {
  default = "t2.micro"
  type    = string
}


variable "eks_min_scale" {
  default = 1
}

variable "eks_desired_scale" {
  default = 3
}
variable "eks_max_scale" {
  default = 5
}
