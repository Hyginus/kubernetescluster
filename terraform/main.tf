# Terraform version ####################################

terraform {
  required_version = ">= 0.12"

  required_providers {
    kubectl = {
      source  = "gavinbunney/kubectl"
      version = ">= 1.7.0"
    }
  }
}


# cluster data ##################################################

data "aws_availability_zones" "available" {}

data "aws_eks_cluster" "cluster" {
  name = module.eks.cluster_id
}

data "aws_eks_cluster_auth" "cluster" {
  name = module.eks.cluster_id
}


# provider###################################################

provider "aws" {
  region = var.region
}

provider "helm" {
  kubernetes {
    host                   = data.aws_eks_cluster.cluster.endpoint
    cluster_ca_certificate = base64decode(data.aws_eks_cluster.cluster.certificate_authority.0.data)
    token                  = data.aws_eks_cluster_auth.cluster.token
  }
}

provider "kubernetes" {
  host                   = data.aws_eks_cluster.cluster.endpoint
  cluster_ca_certificate = base64decode(data.aws_eks_cluster.cluster.certificate_authority.0.data)
  token                  = data.aws_eks_cluster_auth.cluster.token
  # load_config_file       = false
  # version                = "~> 1.12"
}

# Kubectl provider for creating argocd namespace and installing argocd

provider "kubectl" {
  host                   = module.gke_auth.host
  cluster_ca_certificate = module.gke_auth.cluster_ca_certificate
  token                  = module.gke_auth.token
  load_config_file       = false
}

data "kubectl_file_documents" "namespace" {
  content = file("../manifests/argocd/namespace.yaml")
}

data "kubectl_file_documents" "argocd" {
  content = file("../manifests/argocd/install.yaml")
}

resource "kubectl_manifest" "namespace" {
  count              = length(data.kubectl_file_documents.namespace.documents)
  yaml_body          = element(data.kubectl_file_documents.namespace.documents, count.index)
  override_namespace = "argocd"
}

resource "kubectl_manifest" "argocd" {
  depends_on = [
    kubectl_manifest.namespace,
  ]
  count              = length(data.kubectl_file_documents.argocd.documents)
  yaml_body          = element(data.kubectl_file_documents.argocd.documents, count.index)
  override_namespace = "argocd"
}

data "kubectl_file_documents" "my-nginx-app" {
  content = file("../manifests/argocd/my-nginx-app.yaml")
}

resource "kubectl_manifest" "my-nginx-app" {
  depends_on = [
    kubectl_manifest.argocd,
  ]
  count              = length(data.kubectl_file_documents.my-nginx-app.documents)
  yaml_body          = element(data.kubectl_file_documents.my-nginx-app.documents, count.index)
  override_namespace = "argocd"
}


# VPC ###################################################################

locals {
  project_name = "Ydata Test"
}

resource "random_string" "suffix" {
  length  = 8
  special = false
}

module "vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "2.66.0"

  name                 = "${local.project_name}-vpc"
  cidr                 = var.vpc_subnet_cidr
  azs                  = data.aws_availability_zones.available.names
  private_subnets      = var.private_subnet_cidr
  public_subnets       = var.public_subnet_cidr
  enable_nat_gateway   = true
  single_nat_gateway   = true
  enable_dns_hostnames = true

  tags = {
    Name                                            = "${var.eks_cluster_name}-vpc"
    "kubernetes.io/cluster/${var.eks_cluster_name}" = "shared"
  }

  public_subnet_tags = {
    Name                                            = "${var.eks_cluster_name}-eks-public"
    "kubernetes.io/cluster/${var.eks_cluster_name}" = "shared"
    "kubernetes.io/role/elb"                        = "1"
  }

  private_subnet_tags = {
    Name                                            = "${var.eks_cluster_name}-eks-private"
    "kubernetes.io/cluster/${var.eks_cluster_name}" = "shared"
    "kubernetes.io/role/internal-elb"               = "1"
  }
}


# Security group ##########################################################


resource "aws_security_group" "worker_one" {
  name_prefix = "worker_one"
  vpc_id      = module.vpc.vpc_id

  ingress {
    from_port = 22
    to_port   = 22
    protocol  = "tcp"

    cidr_blocks = [
      "10.0.0.0/8",
    ]
  }

  ingress {
    from_port = 0
    to_port   = 80
    protocol  = "tcp"

    cidr_blocks = [
      "10.0.0.0/8",
    ]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
}


# route_53 ########################################################################


locals {
  # https://github.com/hashicorp/terraform-provider-kubernetes/issues/942#issuecomment-731516100

  # "a0ccf7496f46842639b2d0f8c98adfdb-83188769.us-east-1.elb.amazonaws.com"
  #lb_name_parts = split("-", split(".", data.kubernetes_service.service_ingress.status.0.load_balancer.0.ingress.0.hostname).0)
}

# https://www.reddit.com/r/aws/comments/o7oqr0/how_do_i_get_the_domain_name_of_the_load_balancer/
#'[a0ccf7496f46842639b2d0f8c98adfdb]'
# data "aws_lb" "lb" {
#     # name = regex(
#     #       "(^[^-]+)",
#     #       data.kubernetes_service.service_ingress.status[0].load_balancer[0].ingress[0].hostname
#     #     )[0]
#     # name =  "a82455920c5c847659c8e9d216b301c9"
# }


resource "aws_route53_zone" "ydata_kubeflow" {
  name = "ydata.ml"
}

resource "aws_route53_record" "ydata_www" {
  allow_overwrite = true
  name            = "kubeflow.ydata.ml"
  type            = "A"
  zone_id         = aws_route53_zone.ydata_kubeflow.zone_id

  alias {
    name = data.kubernetes_service.service_ingress.status.0.load_balancer.0.ingress.0.hostname
    # [TODO]: Figure out how to make this dynamic
    # zone_id                = "${data.aws_lb.lb.zone_id}" # LB zone
    zone_id                = "Z3AADJGX6KTTL2" # LB zone
    evaluate_target_health = false
  }
}

output "name_servers" {
  value = aws_route53_zone.ydata_kubeflow.name_servers
}

#EKS Cluster #####################################################################

module "eks" {
  source          = "terraform-aws-modules/eks/aws"
  cluster_name    = var.eks_cluster_name
  cluster_version = var.eks_k8s_version
  subnets         = module.vpc.private_subnets
  version         = "17.24.0"

  tags = {
    managed-by = "terraform"
  }

  vpc_id = module.vpc.vpc_id

  workers_group_defaults = {
    root_volume_type = "gp2"
  }

  worker_groups = [
    {
      name                          = "worker-1"
      instance_type                 = var.eks_instance_type
      asg_desired_capacity          = var.eks_desired_scale
      asg_max_size                  = var.eks_max_scale
      asg_min_size                  = var.eks_min_scale
      autoscaling_enabled           = true
      additional_security_group_ids = [aws_security_group.worker_one.id]
    },
    {
      name                          = "worker-2"
      instance_type                 = var.eks_instance_type
      asg_desired_capacity          = var.eks_desired_scale
      asg_max_size                  = var.eks_max_scale
      asg_min_size                  = var.eks_min_scale
      autoscaling_enabled           = true
      additional_security_group_ids = [aws_security_group.worker_one.id]
      taints = [
        {
          key    = "dedicated"
          value  = "statefulset"
          effect = "NO_SCHEDULE"
        }
      ]
    }
  ]
}

# LoadBalancer ###########################################################


resource "helm_release" "ingress" {
  name       = "ingress"
  chart      = "kong"
  repository = "https://charts.konghq.com"

  set {
    name  = "ingressController.installCRDs"
    value = "false"
  }
  set {
    name  = "admin.enabled"
    value = "true"
  }
  set {
    name  = "admin.http.enabled"
    value = "true"
  }
}

data "kubernetes_service" "service_ingress" {
  metadata {
    name      = "ingress-kong-proxy"
    namespace = "default"
  }

  depends_on = [helm_release.ingress]
}

output "KongLoadBalancerIP" {
  value = data.kubernetes_service.service_ingress.status.0.load_balancer.0.ingress.0.hostname
}

# configuration using shell ########################################################

module "update_kubeconfig" {
  source     = "./modules/shell"
  command    = "aws eks update-kubeconfig --name ${var.eks_cluster_name} --region ${var.region}"
  depends_on = [module.eks]
}

#module "install_all" {
# source     = "./modules/shell"
# command    = "cd ../manifests && sh install.sh"
# depends_on = [module.update_kubeconfig]
#}

# Output ###############################################################

output "eks_cluster_iam_role_arn" {
  description = "ARN of the EKS cluster Security Group"
  value       = module.eks.cluster_iam_role_arn
}

output "eks_cluster_iam_role_name" {
  description = "Name of the EKS cluster Security Group"
  value       = module.eks.cluster_iam_role_name
}

